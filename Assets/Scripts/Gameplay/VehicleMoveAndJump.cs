﻿using System;
using System.Collections;
using Core;
using UnityEngine;

namespace Gameplay
{
    public class VehicleMoveAndJump : MonoBehaviour
    {
        public float maxSpeed = -1000f;
        public float jumpForce = 10f;
        public float checkRadius = 0.5f;
        public LayerMask whatIsGround;

        private GameObject _vehicle;
        private Rigidbody2D _rb;

        [Space(10)]
        [Header("Autopilot")]
        public bool isAutopilot;
        public float[] timeToJump;
        public float[] timeToMove;
        private bool _doNothing;
        private float _newTimeToJump;
        private float _timer;
        private float _maxBackSpeed = 1500f;
        private float _acceleration = 250f;
        private float _deacceleration = -100f;
        private float _gravity = 1f;
        private float _angleCar;
        private Transform[] _wheels;
        private WheelJoint2D[] _wheelJoints;
        private JointMotor2D[] _wheelsMotors;
        public static bool IsGrounded;
        public static bool IsMove;


        public void Start()
        {
            _vehicle = VehicleModel.Vehicle;
            _rb = _vehicle.GetComponent<Rigidbody2D>();

            _timer = 0f;
            _newTimeToJump = HelpUtils.Rand(timeToJump);
            
            _wheelJoints = _vehicle.GetComponents <WheelJoint2D>();
            
            _wheels = new Transform[_wheelJoints.Length];
            for (var j = 0; j < _wheelJoints.Length; j++)
                _wheels[j] = _vehicle.transform.GetChild(0).GetChild(j);
            
            _wheelsMotors = new JointMotor2D[_wheelJoints.Length];

            var i = 0;
            foreach (var wheelJoint in _wheelJoints)
            { _wheelsMotors[i] = wheelJoint.motor; i++; }
        }

        private void Update()
        {
            foreach (var check in _wheels)
            {
                if (!Physics2D.OverlapCircle(check.position, checkRadius, whatIsGround))
                {
                    IsGrounded = false; continue;
                }
                IsGrounded = true; break;
            }

            for (var j = 0; j < _wheelsMotors.Length; j++)
                _wheelsMotors[j].motorSpeed = _wheelsMotors[0].motorSpeed;
            
            _angleCar = _vehicle.transform.localEulerAngles.z;
    
            if(_angleCar >= 180)
                _angleCar -= 360;
            
            // jump
            if (IsMove && Input.GetMouseButtonUp(0) && IsGrounded && !isAutopilot)
            {   
                _rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            }
            else if (_timer > _newTimeToJump && !_doNothing && IsGrounded && isAutopilot)
            {
                _rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
                StartCoroutine(DelayBeforeMove());
            }

            // move Ahead
            if (IsMove && ((Input.GetMouseButton(0) && IsGrounded && !isAutopilot) || (_timer <= _newTimeToJump && !_doNothing && IsGrounded && isAutopilot)))
            {
                _wheelsMotors[0].motorSpeed =
                    Mathf.Clamp(
                        _wheelsMotors[0].motorSpeed - (_acceleration - _gravity * Mathf.PI * (_angleCar / 180) * 100) *
                        Time.deltaTime, maxSpeed, _maxBackSpeed);
            }

            // nothing click
            // deACCELERSTION
            if (IsMove && ((!Input.GetMouseButton(0) && IsGrounded && _wheelsMotors[0].motorSpeed < 0) ||
                (!Input.GetMouseButton(0) && IsGrounded && Math.Abs(_wheelsMotors[0].motorSpeed) < .001f && _angleCar < 0)))
            {
                _wheelsMotors[0].motorSpeed =
                    Mathf.Clamp(
                        _wheelsMotors[0].motorSpeed - (_deacceleration - _gravity * Mathf.PI * (_angleCar / 180) * 100) *
                        Time.deltaTime, maxSpeed, 0);
            }
            // ACCELERSTION
            else if (IsMove && ((!Input.GetMouseButton(0) && IsGrounded && _wheelsMotors[0].motorSpeed > 0) ||
                     (!Input.GetMouseButton(0) && IsGrounded && Math.Abs(_wheelsMotors[0].motorSpeed) < .001f && _angleCar > 0)))
            {
                _wheelsMotors[0].motorSpeed =
                    Mathf.Clamp(
                        _wheelsMotors[0].motorSpeed - (-_deacceleration - _gravity * Mathf.PI * (_angleCar / 180) * 100) *
                        Time.deltaTime, 0, _maxBackSpeed);
            }
            
            var i = 0;
            foreach (var wheelJoint in _wheelJoints)
            { wheelJoint.motor = _wheelsMotors[i]; i++; }
            
            _timer += Time.deltaTime;
        }
        
        private IEnumerator DelayBeforeMove()
        {
            _doNothing = true;
            yield return new WaitForSeconds(HelpUtils.Rand(timeToMove));
            _doNothing = false;
            _timer = 0;
            _newTimeToJump = HelpUtils.Rand(timeToJump);
        }

        public void Activator() =>
            enabled = !isActiveAndEnabled;
        
        public void ScriptEnabled() =>
            enabled = true;
        
        public void ScriptDisabled() =>
            enabled = false;
    }
    
}
