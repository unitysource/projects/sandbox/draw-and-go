﻿using System.Collections;
using Core;
using UnityEngine;
using UnityEngine.UI;

namespace Gameplay.Ui
{
    public class DrawVehicleUi : MonoBehaviour
    {
        [SerializeField] private Canvas _drawCanvas;
        [SerializeField] private Animator _drawPanel;
        
        [SerializeField] private Slider _sliderWheelsCount;


        [SerializeField] private GameObject[] _bigVehicles;
        
        private LineRenderer _bodyCur;
        private short _addVehCellNumber;

        private static readonly int IntJoint = Animator.StringToHash("IntJoint");
        private byte _activeCell;


        private VehiclesSystem _vehiclesSystem;
        
        private void Start()
        {
            _drawCanvas.enabled = false;
            LoadVehicles();
            StartCoroutine(WaitPlease());
            // Debug.Log(Application.persistentDataPath);
            _vehiclesSystem = new VehiclesSystem();
        }

        void LoadVehicles()
        {
            // VehiclesSystem.LoadAllVehicles();
            // var vehicleObj = SaveLoadVehicle.SavedVehicles[0];
            // var line = Instantiate(lineRender);
            // line.GetComponent<LineRenderer>().positionCount = vehicleObj.points.Count;
            // var points = new Vector3[vehicleObj.points.Count];
            // Debug.Log(vehicleObj.points);
            // foreach (var t in vehicleObj.points)
            // Debug.Log(t);

            // points[i] = new Vector3(2*i, );
            // line.GetComponent<LineRenderer>().SetPositions(points);

            foreach (var vehicleObj in _vehiclesSystem.SavedVehicles)
            {
                AddVehicleToChoosePanel(vehicleObj);
                _addVehCellNumber++;
            }

        }

        private IEnumerator WaitPlease()
        {
            yield return null;
            _bodyCur = GameObject.FindWithTag("Vehicle").GetComponent<LineRenderer>();
        }

        public void OpenDrawWindow()
        {
            Time.timeScale = 0;
            _drawCanvas.enabled = true;
            _drawPanel.SetInteger(IntJoint, 1);
            VehicleMoveAndJump.IsMove = false;
            StartCoroutine(WaitPleaseLong());
        }

        private IEnumerator WaitPleaseLong()
        {
            yield return new WaitForRealSeconds(1.2f);
            _bodyCur.gameObject.SetActive(true);
        }

        public void BackToChooseWindow()
        {
            _drawPanel.SetInteger(IntJoint, 2);
            InitVehicle();
            StartCoroutine(ResumeGameIe());
        }

        private void InitVehicle()
        {
            var line = DrawVehicle.NewVehicleLine;
            if (line is null) return;
            if (_vehiclesSystem.SavedVehicles.Count >= 9) return;

            var wheelsCount = (byte) _sliderWheelsCount.value;
            Debug.Log("wheelCount: " + wheelsCount);

            _vehiclesSystem.CreateVehicle(line, wheelsCount);
            _vehiclesSystem.SaveVehicle();

            AddVehicleToChoosePanel();

            DrawVehicle.NewVehicleLine = null;

            // if (_bodies.Any(b => b.GetPositions(new Vector3[b.positionCount]) == newBody.GetPositions(new Vector3[newBody.positionCount]))) return;

            // var veh = Instantiate(, tr);
            // veh.transform.localScale /= 4.5f;
            // var rectTr = tr.gameObject.GetComponent<RectTransform>();
            // veh.transform.localPosition = rectTr.position;
        }
        

        private IEnumerator ResumeGameIe()
        {
            yield return new WaitForRealSeconds(1.2f);
            _drawCanvas.enabled = false;
            Time.timeScale = 1;
            VehicleMoveAndJump.IsMove = true;

            DrawVehicle.Points.Clear();
            _bodyCur.positionCount = 1;
            _bodyCur.SetPosition(0, Vector3.zero);
            _bodyCur.gameObject.SetActive(false);
        }

        public void SelectVehicle(int id)
        {
            if(_chooseCells[id].transform.childCount == 0) return;
            WorkWithInstance();
            var vehicle = _chooseCells[id].transform.GetChild(0);
            int wheelsCount = vehicle.transform.GetChild(0).childCount;
            var line = vehicle.transform.GetChild(1).GetComponent<LineRenderer>();
            var mahina = BIG_VEHICLES[wheelsCount - 1];
            mahina.SetActive(true);
            var instance = Instantiate(line, mahina.transform);
            var transform1 = instance.transform;
            transform1.localPosition = Vector3.zero;
            transform1.localScale = Vector3.one;
            _activeCell = (byte)id;
        }

        public void DestroyVehicle()
        {
            WorkWithInstance();
            if(_chooseCells[_activeCell].transform.childCount > 0)
                Destroy(_chooseCells[_activeCell].transform.GetChild(0).gameObject, 0.01f);
            _vehiclesSystem.Destroy(_activeCell);
        }

        private void WorkWithInstance()
        {
            foreach (var instVeh in BIG_VEHICLES)
            {
                var childCount = instVeh.transform.childCount;
                if(childCount > 1) Destroy(instVeh.transform.GetChild(1).gameObject, .01f);
                instVeh.SetActive(false);
            }
        }
    }
}