﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class SlidersUi : MonoBehaviour
{
    public Slider[] sliders;
    
    [Header("Count of wheels")]
    public Sprite[] drawFields;
    public Text countOfWheels;
    [Space(10)]
    [Header("BrushColor")] 
    public Color[] colors;
    public Image colorImg;
    
    [Space(10)]
    [Header("BrushThick")] 
    public int[] widths;
    public Image thickImg;

    private Image _drawFieldImg;
    private LineRenderer _drawingVehicleLine;
    
    private void Start()
    {
        _drawFieldImg = GameObject.FindWithTag("DrawField").transform.parent.GetComponent<Image>();
        StartCoroutine(LateStart());
    }

    private IEnumerator LateStart()
    {
        yield return null;
        _drawingVehicleLine = GameObject.FindWithTag("Vehicle").GetComponent<LineRenderer>();
    }

    public void ChangeWheelCount(float value)
    {
        var val = Mathf.RoundToInt(value);
        countOfWheels.text = Convert.ToString(value, CultureInfo.InvariantCulture);
        _drawFieldImg.sprite = drawFields[val-1];
    }
    
    public void ChangeVehicleColor(float value)
    {
        var val = Mathf.RoundToInt(value);
        colorImg.color = colors[val];
        _drawingVehicleLine.startColor = _drawingVehicleLine.endColor = colors[val];
    }
    
    public void ChangeVehicleThick(float value)
    {
        var val = Mathf.RoundToInt(value);
        var rectTransform = thickImg.rectTransform;
        rectTransform.sizeDelta = new Vector2(widths[val-1], rectTransform.sizeDelta.y);
        var endWidth = (float)widths[val-1] / 70;
        Debug.Log(endWidth);
        _drawingVehicleLine.startWidth = _drawingVehicleLine.endWidth = endWidth;
    }
}
