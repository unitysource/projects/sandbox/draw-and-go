﻿using System.Collections;
using Core;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Gameplay.Ui
{
    public class GameOver : MonoBehaviour
    {
        public Canvas gameOverCanvas;
        public Animator gameOverPanel;
    
        private static readonly int IntJoint = Animator.StringToHash("IntJoint");

        private void Start() => gameOverCanvas.enabled = false;

        public void GameOverTheGame()
        {
            Time.timeScale = 0;
            gameOverCanvas.enabled = true;
            gameOverPanel.SetInteger(IntJoint, 1);
            VehicleMoveAndJump.IsMove = false;
        }

        public void RestartGame() => StartCoroutine(HelpUtils.LoadSceneToString(SceneManager.GetActiveScene().name, gameOverPanel, IntJoint));
        
        public void GoHome() => StartCoroutine(HelpUtils.LoadSceneToString("MainMenuScene", gameOverPanel, IntJoint));
    }
}
