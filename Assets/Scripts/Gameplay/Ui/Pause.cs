﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Gameplay.Ui
{
    public class Pause : MonoBehaviour
    {
        public Canvas pausedCanvas;
        public Animator pausedPanel;

        private static readonly int IntJoint = Animator.StringToHash("IntJoint");

        private void Start() => pausedCanvas.enabled = false;
        public void PauseGame()
        {
            Time.timeScale = 0;
            pausedCanvas.enabled = true;
            pausedPanel.SetInteger(IntJoint, 1);
            VehicleMoveAndJump.IsMove = false;
        }
        public void ResumeGame()
        {
            pausedPanel.SetInteger(IntJoint, 2);
            StartCoroutine(ResumeGameIe());
        }
        private IEnumerator ResumeGameIe()
        {
            yield return new WaitForRealSeconds(1.2f);
            pausedCanvas.enabled = false;
            Time.timeScale = 1;
            VehicleMoveAndJump.IsMove = true;
        }
        public void GoHome() => StartCoroutine(HelpUtils.LoadSceneToString("MainMenuScene", pausedPanel, IntJoint));

        public void RestartGame() => StartCoroutine(HelpUtils.LoadSceneToString(SceneManager.GetActiveScene().name, pausedPanel, IntJoint));
    }
}
