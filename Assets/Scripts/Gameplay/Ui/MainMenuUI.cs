﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Gameplay.Ui
{
   public class MainMenuUI : MonoBehaviour
   {
      public void StartGame()
      {
         StartCoroutine(StartGameIe());
      }

      IEnumerator StartGameIe()
      {
         yield return new WaitForSeconds(.2f);
         SceneManager.LoadScene("GameplayScene");
      }
   }
}
