﻿using Core;
using UnityEngine;

namespace Gameplay.Ui
{
    public class PanelViewSystem : MonoBehaviour
    {
        [SerializeField] private GameObject[] _panelCells = new GameObject[9];

        [SerializeField] private LineRenderer _lineRender;

        [SerializeField] private GameObject[] vehWithoutBodies;

        public void DisplayOnPanel(VehicleObj vehicleObj)
        {
            var wheelsOnlyBody = GetObjectByWheelsCount(vehicleObj.wheelsCount);
            var line = Instantiate(_lineRender, wheelsOnlyBody.transform);
            LineParse(line, vehicleObj);

            // var child = vehWithoutBody.transform.GetChild(0);
            // child.localScale *= 5;

            // var localPosition = child.localPosition;
            // localPosition = new Vector3(localPosition.x, localPosition.y - 160f, localPosition.z);
            // child.localPosition = localPosition;

            // if (vehicleObj == null)
            //     DrawVehicle.NewVehicleLine.transform.SetParent(vehWithoutBody.transform, false);


            // Debug.Log(Application.persistentDataPath);
            // var vehWithoutBody = Instantiate(vehWithoutBodies[Current.wheelsCount - 1],
            //     chooseCells[firstEmptyIndex].transform);

            // vehWithoutBody.transform.localScale /= 3;
        }


        private void LineParse(LineRenderer line, VehicleObj vehicleObj)
        {
            // Debug.Log(vehicleObj.points[0]);
            line.positionCount = vehicleObj.points.Length;
            for (var i = 0; i < vehicleObj.points.Length; i++)
            {
                var point = (Vector3) vehicleObj.points[i];
                line.SetPosition(i, point);
            }

            line.startColor = line.endColor = HelpUtils.ParseColor(vehicleObj.color);
            line.startWidth = line.endWidth = vehicleObj.width;
        }

        private GameObject GetObjectByWheelsCount(byte wheelsCount) =>
            vehWithoutBodies[wheelsCount - 1];
    }
}