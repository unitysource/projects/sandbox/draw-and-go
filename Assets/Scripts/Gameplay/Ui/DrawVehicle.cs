﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core;
using Mechanics.Vehicle;
using Microsoft.Unity.VisualStudio.Editor;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Gameplay.Ui
{
    public class DrawVehicle : MonoBehaviour
    {
        public GameObject linePrefab;
        [Tooltip("Min distance between this and before point to create line")]
        public float minDistance;
        public int maxPoints=300;

        private GameObject _curLine;
        private LineRenderer _lineRenderer;
        // private EdgeCollider2D _collider;
        public static List<Vector2> Points;
        private GameObject _drawField;
        private Canvas _drawFieldRootCanvas;
        private Camera _main;
        private Vector3[] _drawFieldCorners;

        public static LineRenderer NewVehicleLine;
        // private List<LineRenderer> _bodies;
        
        private void Start()
        {
            _main = Camera.main;
            if (_main == null) return;
            _drawField = GameObject.FindWithTag("DrawField");
            _drawFieldCorners = new Vector3[4];
            _drawField.GetComponent<RectTransform>().GetWorldCorners(_drawFieldCorners); // 0-l_b, 1-l_t, 2-r_t, 3-r_b 
            // https://csharp.hotexamples.com/examples/UnityEngine/RectTransform/GetWorldCorners/php-recttransform-getworldcorners-method-examples.html
            
            Points = new List<Vector2>();
            _curLine = Instantiate(linePrefab, _drawField.transform.parent, true);
            _curLine.tag = "Vehicle";
            _lineRenderer = _curLine.GetComponent<LineRenderer>();
            _lineRenderer.sortingOrder = 4;
            // _collider = _curLine.AddComponent<EdgeCollider2D>();
            // _collider.edgeRadius = .1f;
            Points.Clear();
            _drawFieldRootCanvas = _drawField.transform.root.GetComponent<Canvas>();
            StartCoroutine(SetActiveObjIe());
        }

        private IEnumerator SetActiveObjIe()
        {
            yield return new WaitForSeconds(.5f);
            _curLine.SetActive(false);
        }

        private void Update()
        {
            if(!_drawFieldRootCanvas.isActiveAndEnabled 
               || Points.Count >= maxPoints) return;
            
            var tempPos = _main.ScreenToWorldPoint(Input.mousePosition);
            if(tempPos.x <= _drawFieldCorners[0].x || 
               tempPos.x >= _drawFieldCorners[2].x ||
               tempPos.y <= _drawFieldCorners[0].y || 
               tempPos.y >= _drawFieldCorners[2].y) return;
            
            if (Input.GetMouseButtonDown(0))
            {
                CreateLine();
            }
            
            if (!Input.GetMouseButton(0)) return;
            if(Vector2.Distance(tempPos, Points[Points.Count-1]) > minDistance)
                CreateNewLine(tempPos);
        }

        private void CreateLine()
        {
            if (_lineRenderer.loop) _lineRenderer.loop = false;
            Points.Add(_main.ScreenToWorldPoint(Input.mousePosition));
            // _points.Add(_main.ScreenToWorldPoint(Input.mousePosition));
            if (Points.Count <= 1)
                _lineRenderer.SetPosition(0, Points[Points.Count - 1]);
            else
            {
                var positionCount = _lineRenderer.positionCount;
                positionCount++;
                _lineRenderer.positionCount = positionCount;
                _lineRenderer.SetPosition(positionCount-1, Points[Points.Count-1]);
            }
            // _lineRenderer.SetPosition(1, _points[1]);
            // _collider.points = _points.ToArray();
            
            

            if(!_curLine.activeSelf)
                _curLine.SetActive(true);
        }

        private void CreateNewLine(Vector2 fingerPos)
        {
            Points.Add(fingerPos);
            var positionCount = _lineRenderer.positionCount;
            positionCount++;
            _lineRenderer.positionCount = positionCount;
            _lineRenderer.SetPosition(positionCount-1, fingerPos);
            // _collider.points = _points.ToArray();
        }

        public void DestroyAllPoints()
        {
            if(Points.Count <= 1) return;
            if(_lineRenderer.loop) _lineRenderer.loop = false;
            // _curLine.SetActive(true);
            _lineRenderer.positionCount = 1;
            _lineRenderer.SetPosition(0, Vector3.zero);
            Points.Clear();
            // _collider.points = _points.ToArray();
            _curLine.SetActive(false);
        }

        public void DoneVehicle()
        {
            if(Points.Count <= 1) return;
            if (_lineRenderer.loop) return;
            
            _lineRenderer.loop = true;
            var newBody = GameObject.FindWithTag("Vehicle").GetComponent<LineRenderer>();
            // if (NewVehicle is null)
            // {
            //     NewVehicle = Instantiate(newBody);
            //     NewVehicle.gameObject.SetActive(false);
            // }
            // else NewVehicle = newBody; 
            
            NewVehicleLine = Instantiate(newBody, transform);
            NewVehicleLine.sortingOrder = 0;
            NewVehicleLine.tag = "Untagged";
            var transform1 = NewVehicleLine.transform;
            var position = transform1.position;
            transform1.localPosition = new Vector3(position.x, position.y, 0);

            // toDo...
        }
        
        public void StepBack()
        {
            if(Points.Count <= 1) return;
            if(_lineRenderer.loop) _lineRenderer.loop = false;
            _lineRenderer.positionCount = _lineRenderer.positionCount-1;
            Points.RemoveAt(Points.Count-1);
            if(Points.Count <= 1) _curLine.SetActive(false);
        }
    }
}
