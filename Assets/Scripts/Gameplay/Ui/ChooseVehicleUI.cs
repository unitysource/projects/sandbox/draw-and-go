﻿using System.Collections;
using Core;
using UnityEngine;

namespace Gameplay.Ui
{
    public class ChooseVehicleUI : MonoBehaviour
    {
        // public Canvas chooseVehCanvas;
        public Animator chooseVehPanel;
    
        private static readonly int IntJoint = Animator.StringToHash("IntJoint");

        private void Start()
        {
            Time.timeScale = 0;
            StartCoroutine(StopTime());
        }

        private IEnumerator StopTime()
        {
            chooseVehPanel.SetInteger(IntJoint, 1);
            yield return new WaitForRealSeconds(1.2f);
            Time.timeScale = 1;
        }
    
        public void BackToMainMenu() => StartCoroutine(HelpUtils.LoadSceneToString("MainMenuScene", chooseVehPanel, IntJoint));

        public void DestroyVehicle()
        {
            foreach (var vehicle in GameObject.FindGameObjectsWithTag("Player"))
            {
                //toDo animation and ...
                Destroy(vehicle, .01f);
            }
        }
    }
}
