﻿using Core;
using UnityEngine;

namespace Mechanics
{
    public class RoadComplexity : MonoBehaviour
    {
        [Tooltip("Element 0 - it is a minStep, el1 - maxStep (for vehicle with 1 wheel)")] 
        [SerializeField] private Vector2[] roadStepFor1Wheel;
        // public Vector2[] RoadStepFor1Wheel => roadStepFor1Wheel;

        [Space(10)]
    
        [Tooltip("Element 0 - it is a minStep, el1 - maxStep (for vehicle with 2 wheels)")]
        [SerializeField] private Vector2[] roadStepFor2Wheels;
        // public Vector2[] RoadStepFor2Wheel => roadStepFor2Wheels;

        [Space(10)] [Tooltip("Element 0 - it is a minStep, el1 - maxStep (for vehicle with 3 wheels)")] [SerializeField]
        private Vector2[] roadStepFor3Wheels;
        // public Vector2[] RoadStepFor3Wheel => roadStepFor3Wheels;

        private Transform _vehicleWheelsChild;
        private RoadGenerator _road;
    
        public void Start()
        {
            _vehicleWheelsChild = VehicleModel.Vehicle.transform.GetChild(0);
            ComplexityRoad();
        }

        void ComplexityRoad()
        {
            var wheelsCount = _vehicleWheelsChild.childCount;
            var minStep = RoadGenerator.MinStep;
            var maxStep = RoadGenerator.MaxStep;
            switch (wheelsCount)
            {
                case 1:
                    minStep = roadStepFor1Wheel[0];
                    maxStep = roadStepFor1Wheel[1];
                    break;
                case 2:
                    minStep = roadStepFor2Wheels[0];
                    maxStep = roadStepFor2Wheels[1];
                    break;
                case 3:
                    minStep = roadStepFor3Wheels[0];
                    maxStep = roadStepFor3Wheels[1];
                    break;
                default:
                    Debug.Log(wheelsCount);
                    break;
            }
        
            RoadGenerator.MinStep = minStep;
            RoadGenerator.MaxStep = maxStep;
        }
        
        public void Activator() =>
            enabled = !isActiveAndEnabled;
        
        public void ScriptEnabled() =>
            enabled = true;
        
        public void ScriptDisabled() =>
            enabled = false;
    }
}
