﻿using Core;
using UnityEngine;
using UnityEngine.UI;

namespace Mechanics
{
    public class ScoreAdd : MonoBehaviour
    {
        public Text scoreText;
        public float speedScore;

        // private float _score;
    
        private Transform _vehicleInScene;
        private  Vector2 _startPosVehicle;
        void Start()
        {
            _vehicleInScene = VehicleModel.Vehicle.transform; 
            _startPosVehicle = _vehicleInScene.transform.position;
            PlayerPrefs.SetInt("Step", 0);
        }

        void Update()
        {
            var distance = (int)(HelpUtils.Pythagoras(_vehicleInScene.position, _startPosVehicle) * speedScore);
            if (distance > PlayerPrefs.GetInt("Step"))
                PlayerPrefs.SetInt("Step", distance);
            scoreText.text = $"Score: {PlayerPrefs.GetInt("Step")}";
        }
        
        public void Activator() =>
            enabled = !isActiveAndEnabled;
        
        public void ScriptEnabled() =>
            enabled = true;
        
        public void ScriptDisabled() =>
            enabled = false;
    }
}
