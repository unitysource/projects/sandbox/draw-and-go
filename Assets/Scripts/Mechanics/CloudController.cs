﻿using System;
using System.Collections.Generic;
using Core;
using UnityEngine;
using UnityEngine.Diagnostics;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

// using Random = System.Random;

namespace Mechanics
{
    public class CloudController : MonoBehaviour
    {
        public float[] delayBetweenClouds;
        public float[] speed;
        public float[] height;
        public float timeToDestroy;
        public List<Sprite> clouds;
        public GameObject cloudPrefab;
        public Transform parent;

        private float _timer;
        private Camera _main;
        private Vector2 _cameraPos;
        private Vector2 _screenScale;

        private GameObject _cloudParent;

        private void Start()
        {
            _timer = 0f;
            _main = Camera.main;
            if (_main == null) return;
            if (SceneManager.GetActiveScene().name == "CreateAndChooseVehicleScene")
            {
                _cloudParent = Instantiate(cloudPrefab, Vector3.up * 30f, Quaternion.identity);
                _cloudParent.AddComponent<Rigidbody2D>();
                _cloudParent.GetComponent<Rigidbody2D>().gravityScale = 0;
                _cloudParent.transform.SetParent(parent);
                _cloudParent.SetActive(false);
                
                VehicleModel.ScreenScale = _main.ViewportToWorldPoint (new Vector2 (1,1)); // top-right corner;
                _screenScale = VehicleModel.ScreenScale;
            }
            else
            {
                var cloud = Instantiate(cloudPrefab, parent);
                _cameraPos = _main.transform.position;
                var newHeight = HelpUtils.Rand(height);
                cloud.transform.position = new Vector2(_cameraPos.x + (_main.rect.width / 2 + 10f),
                    _cameraPos.y + newHeight);
                cloud.transform.localScale = Vector2.one * (newHeight / 3);
                Destroy(cloud, timeToDestroy);
            }
        }

        private void Update()
        {
            if (_timer >= HelpUtils.Rand(delayBetweenClouds))
            {
                if (SceneManager.GetActiveScene().name == "CreateAndChooseVehicleScene")
                {
                    var cloud = Instantiate(_cloudParent, parent);
                    cloud.SetActive(true);
                    var newHeight = Random.Range(_screenScale.y - 4f, _screenScale.y - 2f);
                    cloud.transform.position = new Vector2(_screenScale.x-1.5f, newHeight);
                    cloud.transform.localScale = Vector2.one * (newHeight / 3);
                }
                else
                {
                    var cloud = Instantiate(cloudPrefab, parent);
                    _cameraPos = _main.transform.position;
                    var newHeight = HelpUtils.Rand(height);
                    cloud.transform.position = new Vector2(_cameraPos.x + (_main.rect.width / 2 + 10f),
                        _cameraPos.y + newHeight);
                    cloud.transform.localScale = Vector2.one * (newHeight / 3);
                    Destroy(cloud, timeToDestroy);
                }
                _timer = 0;
            }
            _timer += Time.deltaTime;
        }

        public void Activator() =>
            enabled = !isActiveAndEnabled;
        
        public void ScriptEnabled() =>
            enabled = true;
        
        public void ScriptDisabled() =>
            enabled = false;
    }
}
