﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace Mechanics
{
    public class RoadGenerator : MonoBehaviour
    {
        public GameObject linePrefab;
        private GameObject _vehiclePrefab;
        public static Vector2 MinStep;
        public static Vector2 MaxStep;
        public Vector2 startRoadPos;
        public Vector2 startRoadPos2;

        private GameObject _curLine;
        private LineRenderer _lineRenderer;
        private EdgeCollider2D _edgeCollider;
    
        private Vector2 _curPoint;

        private List<Vector2> _points;
    
        public void Start()
        {
            // if (GameObject.FindGameObjectsWithTag("Road").Length > 0) return;
            _vehiclePrefab = VehicleModel.Vehicle;
            _points  = new List<Vector2>();
            _curPoint = startRoadPos2;
            
            CreateLine();
        
            StartCoroutine(AddLine());
        }

        private void CreateLine()
        {
            _curLine = Instantiate(linePrefab, Vector3.zero, Quaternion.identity);
            _curLine.tag = "Road";
            _curLine.layer = 8;

            _lineRenderer = _curLine.GetComponent<LineRenderer>();
            _edgeCollider = _curLine.AddComponent<EdgeCollider2D>();
            _lineRenderer.useWorldSpace = true;
            _edgeCollider.edgeRadius = .09f;
            _points.Clear();
            _points.Add(startRoadPos);
            _points.Add(startRoadPos2);
            _lineRenderer.positionCount = 2;
            _lineRenderer.SetPosition(0, _points[0]);
            _lineRenderer.SetPosition(1, _points[1]);
            _edgeCollider.points = _points.ToArray();
        }

        void CreateNewLine()
        {
            var nextPoint = CreateNextPoint();
            _points.Add(nextPoint);
            var positionCount = _lineRenderer.positionCount;
            positionCount++;
            _lineRenderer.positionCount = positionCount;
            _lineRenderer.SetPosition(positionCount-1, nextPoint);
            _edgeCollider.points = _points.ToArray();
            _curPoint = nextPoint;
        }

        void RemoveOldPoints(int destroyCount)
        {
            _points.RemoveRange(0, 5);
            var positionCountWithoutOld = _lineRenderer.positionCount-destroyCount;
            var newPositions = new Vector3[positionCountWithoutOld];
        
            for (var i = 0; i < positionCountWithoutOld; i++){
                newPositions[i] = _lineRenderer.GetPosition(i + destroyCount);
            }

            _lineRenderer.positionCount -= destroyCount;
            _lineRenderer.SetPositions(newPositions);
            _edgeCollider.points = _points.ToArray();
        }

        private Vector2 CreateNextPoint() =>
            new Vector2(_curPoint.x + Random.Range(MinStep.x, MaxStep.x),
                _curPoint.y + Random.Range(MinStep.y, MaxStep.y));

        private IEnumerator AddLine()
        {
            while (_vehiclePrefab)
            {
                var vehPos = _vehiclePrefab.transform.GetChild(0).position;

                if (HelpUtils.Pythagoras(_lineRenderer.GetPosition
                    (_lineRenderer.positionCount - 1), vehPos) < 70f)
                    CreateNewLine();
                if (HelpUtils.Pythagoras(_lineRenderer.GetPosition(0),
                    vehPos) > 70f)
                    RemoveOldPoints(5);

                yield return new WaitForSeconds(.2f);
            }
        }
        
        public void Activator() =>
            enabled = !isActiveAndEnabled;
        
        public void ScriptEnabled() =>
            enabled = true;
        
        public void ScriptDisabled() =>
            enabled = false;

        public static void DestroyRoad()
        {
            foreach (var road in GameObject.FindGameObjectsWithTag("Road")) 
                Destroy(road, 01f);
        }
    }
}
