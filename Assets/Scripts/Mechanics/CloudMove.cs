﻿using System;
using Core;
using UnityEngine;

namespace Mechanics
{
    public class CloudMove : MonoBehaviour
    {
        private float _speed;
        private CloudController _controller;

        private void Start()
        {
            _controller = GameObject.Find("CloudsController").GetComponent<CloudController>();
            _speed = 5f / (transform.localScale.x * 2);
            GetComponent<SpriteRenderer>().sprite = HelpUtils.RandIndex(_controller.clouds);
        }

        void Update()
        {
            transform.position += Vector3.left * (Time.deltaTime * _speed);
        }

        // private void OnCollisionEnter(Collision other)
        // {
        //     Debug.Log(other);
        //
        //     if (other.gameObject.CompareTag("CloudWall"))
        //     {
        //         Destroy(gameObject, .1f);
        //     }
        // }
    }
}
