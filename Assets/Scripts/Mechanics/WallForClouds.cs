﻿using UnityEngine;

namespace Mechanics
{
    public class WallForClouds : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Cloud")) 
                Destroy(other.gameObject, .1f);
        }
    }
}
