﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core;
using Mechanics.Vehicle;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace Mechanics
{
    public class VehicleGenerator : MonoBehaviour
    { 
        public List<GameObject> vehiclePrefabs;
        public Vector2 startVehiclePos = Vector2.zero;
        public float bodyBiasUp=1f;
        public float scaleVehicle=.6f;
        public GameObject linePrefab;
        public Vector2 minStep;
        public Vector2 maxStep;
        public int[] maxPointsCount;
        public float delay=.1f;
        public float decreaseLineToVehicle = 2.5f;

        [Space(20)] 
        public Animator blackScreenPanel; 
        public Canvas blackScreenCanvas; 
    
        private GameObject _curLine;
        private LineRenderer _lineRenderer;
        private PolygonCollider2D _polygonCollider;
        private Vector2 _bordersX;
        private Vector2 _bordersY;
        private Vector2 _curPoint;
        private List<Vector2> _points;
        private static readonly int IntJoint = Animator.StringToHash("IntJoint");

        public void Start()
        {
            blackScreenCanvas.enabled = false;
            if (Camera.main != null)
            {
                Vector2 screenScale = Camera.main.ViewportToWorldPoint (new Vector2 (1,1)); // top-right corner
                _bordersX = new Vector2(-screenScale.x*.7f, screenScale.x*.7f);
                _bordersY = new Vector2(-screenScale.y*.7f, screenScale.y*.7f);
            }
            
            _points  = new List<Vector2>();
            _curPoint = new Vector2(Random.Range(-3f, 3f),
                Random.Range(-2.5f, 2.5f));
            CreateLine();
        
            StartCoroutine(AddLine());
        }
        
        void CreateLine()
        {
            _curLine = Instantiate(linePrefab, Vector3.zero, Quaternion.identity);
            _lineRenderer = _curLine.GetComponent<LineRenderer>();
            _lineRenderer.useWorldSpace = false;
            _polygonCollider = _curLine.AddComponent<PolygonCollider2D>();
            _points.Clear();
            _points.Add(_curPoint);
            _lineRenderer.SetPosition(0, _points[0]);
            _polygonCollider.points = _points.ToArray();
        }

        void CreateNewLine()
        {
            var nextPoint = CreateNextPoint();
            while (nextPoint.x < _bordersX[0] || nextPoint.x > _bordersX[1] ||
                   nextPoint.y < _bordersY[0] || nextPoint.y > _bordersY[1])
            {
                nextPoint = CreateNextPoint();
            }
            _points.Add(nextPoint);
            var positionCount = _lineRenderer.positionCount;
            positionCount++;
            _lineRenderer.positionCount = positionCount;
            _lineRenderer.SetPosition(positionCount-1, nextPoint);
            _polygonCollider.points = _points.ToArray();
            _curPoint = nextPoint;
        }
        Vector2 CreateNextPoint() =>
            new Vector2(_curPoint.x + Random.Range(minStep.x, maxStep.x),
                _curPoint.y + Random.Range(minStep.y, maxStep.y));

        IEnumerator AddLine()
        {
            while (_points.Count < HelpUtils.Rand(maxPointsCount))
            {
                CreateNewLine();
                yield return new WaitForSeconds(delay);
            }
            _lineRenderer.loop = true;

            blackScreenCanvas.enabled = true;
            blackScreenPanel.SetInteger(IntJoint, 1);
            yield return new WaitForSeconds(.5f);

            var newVehicle = Instantiate(HelpUtils.RandIndex(vehiclePrefabs), startVehiclePos, Quaternion.identity);
            newVehicle.AddComponent<VehicleMainMenu>();

            var newLine = Instantiate(_curLine, newVehicle.gameObject.transform);
            newLine.transform.localScale = Vector3.one / decreaseLineToVehicle;
            newLine.transform.localPosition = Vector2.up * bodyBiasUp;
            newVehicle.transform.localScale = Vector3.one * scaleVehicle;
            _curLine.SetActive(false);

            // newVehicle.transform.rotation = new Quaternion(0, 0, 180, 0);
            
            blackScreenPanel.SetInteger(IntJoint, 2);
            blackScreenCanvas.enabled = false;
            yield return new WaitForSeconds(.5f);
            blackScreenPanel.SetInteger(IntJoint, 0);
        }

        public void ReCreationVehicle()
        {
            if (_curLine == null) return;
            _lineRenderer.loop = false;
            _curLine.SetActive(true);
            _lineRenderer.positionCount = 1;
            _curPoint = new Vector2(Random.Range(-3f, 3f),
                Random.Range(-2.5f, 2.5f));
            _points.Clear();
            _points.Add(_curPoint);
            _lineRenderer.SetPosition(0, _points[0]);
            _polygonCollider.points = _points.ToArray();
            
            if (_curLine == null) return;
            StartCoroutine(AddLine());
        }
    }
}
