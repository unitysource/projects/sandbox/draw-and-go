﻿using Core;
using UnityEngine;

namespace Mechanics
{
    public class CameraController : MonoBehaviour
    {
        public float smoothDelay=.1f;
        public float biasY=1.5f;
        private Transform _target;
        private Camera _camera;

        public void Start()
        {
            _target = VehicleModel.Vehicle.transform;
            _camera = Camera.main;
            if (_camera != null)
            {
                var transform1 = _camera.transform;
                var position = transform1.position;
                position = new Vector3(position.x, position.y, -10);
                transform1.position = position;
            }
        }

        void FixedUpdate()
        {
            var position = _target.position;
            var tarPos = new Vector3(position.x, position.y+biasY, -10);
            _camera.transform.position = Vector3.Lerp(_camera.transform.position, tarPos, smoothDelay);
        }
        
        public void Activator() =>
            enabled = !isActiveAndEnabled;
        
        public void ScriptEnabled() =>
            enabled = true;
        
        public void ScriptDisabled() =>
            enabled = false;
    }
}
