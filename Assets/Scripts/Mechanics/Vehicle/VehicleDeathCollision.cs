﻿using UnityEngine;

namespace Mechanics.Vehicle
{
    public class VehicleDeathCollision : MonoBehaviour
    {
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.layer != 8) return;
            VehicleDeath.IsOverturned = true;
            Debug.Log(VehicleDeath.IsOverturned);
        }

        private void OnCollisionExit2D(Collision2D other)
        {
            if (other.gameObject.layer == 8) VehicleDeath.IsOverturned = false;
        }
    }
}
