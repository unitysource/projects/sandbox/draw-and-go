﻿using System.Collections;
using Core;
using Gameplay;
using Gameplay.Ui;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Mechanics.Vehicle
{
    public class VehicleDeath : MonoBehaviour
    {
        private float _timer;
        [SerializeField] private float timeToDie = 2f;
        [SerializeField] private GameObject explosionPrefab;
        
        public static bool IsOverturned;
        public static float AnimTime;
        public float animTime=2f;

        public GameObject uIGameOver;
        private GameOver _gameOver;

        void Start()
        {
            if(uIGameOver!=null) 
                _gameOver = uIGameOver.GetComponent<GameOver>();
            AnimTime = animTime;
            _timer = 0;
            IsOverturned = false;
        }

        void FixedUpdate()
        {
            Death();
        }

        void Death()
        {
            if (IsOverturned)
            {
                _timer += Time.deltaTime;
                if (_timer >= timeToDie)
                {
                    Dead();
                }
            }
            else _timer = 0f;
        }

        void Dead()
        {
            var pos = VehicleModel.Vehicle.transform.position;
            Destroy(VehicleModel.Vehicle, .01f);

            if (SceneManager.GetActiveScene().name == "GameplayScene") 
                StartCoroutine(LoadGameOver(pos));
        }

        IEnumerator LoadGameOver(Vector3 pos)
        {
            var explosion = Instantiate(explosionPrefab, pos, Quaternion.identity);
            yield return new WaitForSeconds(AnimTime);
            Destroy(explosion, .001f);
            _gameOver.GameOverTheGame();
        }
        
        public void Activator() =>
            enabled = !isActiveAndEnabled;
        
        public void ScriptEnabled() =>
            enabled = true;
        
        public void ScriptDisabled() =>
            enabled = false;
    }
}
