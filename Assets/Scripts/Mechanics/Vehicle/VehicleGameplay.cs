﻿using Core;
using Gameplay;
using Mechanics.Event;
using UnityEngine;

namespace Mechanics.Vehicle
{
    public class VehicleGameplay : MonoBehaviour
    {
        public static event EventControllerGameplay.MethodContainer EnabledEvent;
        public static event EventControllerGameplay.MethodContainer DisabledEvent;

        private void OnEnable()
        {
            VehicleModel.Vehicle = gameObject;
            VehicleMoveAndJump.IsMove = true;
            EnabledEvent?.Invoke();
        }

        private void OnDestroy()
        {
            DisabledEvent?.Invoke();
        }
    }
}
