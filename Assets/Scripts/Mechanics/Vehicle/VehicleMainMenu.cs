﻿using System.Collections;
using Core;
using Gameplay;
using Mechanics.Event;
using UnityEngine;

namespace Mechanics.Vehicle
{
    public class VehicleMainMenu : MonoBehaviour
    {
        public static event EventControllerMainMenu.MethodContainerMainMenu EnabledEvent;
        public static event EventControllerMainMenu.MethodContainerMainMenu DestroyRoadEvent;
        public static event EventControllerMainMenu.MethodContainerMainMenu RestartGenerateVehicleEvent;
        public static event EventControllerMainMenu.MethodContainerMainMenu FindVehicleEvent;

        
        private void OnEnable()
        {
            VehicleDeath.IsOverturned = false;
            VehicleModel.Vehicle = gameObject;
            FindVehicleEvent?.Invoke();
            EnabledEvent?.Invoke();
            VehicleMoveAndJump.IsMove = true;
        }
        
        private void OnDestroy()
        {
            EnabledEvent?.Invoke();
            DestroyRoadEvent?.Invoke();
            if (Camera.main != null) Camera.main.transform.position = Vector3.forward*-10f; 
            RestartGenerateVehicleEvent?.Invoke();
        }
    }
}
