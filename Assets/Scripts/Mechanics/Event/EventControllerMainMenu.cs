﻿using Gameplay;
using Mechanics.Vehicle;
using UnityEngine;

namespace Mechanics.Event
{
    public class EventControllerMainMenu:MonoBehaviour
    {
        public VehicleMoveAndJump vehicleMove;
        public RoadGenerator roadGenerator;
        public RoadComplexity roadComplexity;
        public CameraController cameraController;
        public CloudController cloudController;
        public VehicleDeath vehicleDeath;
        public VehicleGenerator vehicleGenerator;

        public delegate void MethodContainerMainMenu();
        
        void Awake()
        {
            VehicleMainMenu.RestartGenerateVehicleEvent += vehicleGenerator.ReCreationVehicle;
        
            VehicleMainMenu.EnabledEvent += roadGenerator.Activator;
            VehicleMainMenu.FindVehicleEvent += roadGenerator.Start;
            VehicleMainMenu.DestroyRoadEvent += RoadGenerator.DestroyRoad;
        
            VehicleMainMenu.FindVehicleEvent += roadComplexity.Start;
            VehicleMainMenu.EnabledEvent += roadComplexity.Activator;
        
            VehicleMainMenu.FindVehicleEvent += cameraController.Start;
            VehicleMainMenu.EnabledEvent += cameraController.Activator;
        
            VehicleMainMenu.EnabledEvent += cloudController.Activator;
        
            VehicleMainMenu.EnabledEvent += vehicleDeath.Activator;
        
            VehicleMainMenu.FindVehicleEvent += vehicleMove.Start;
            VehicleMainMenu.EnabledEvent += vehicleMove.Activator;
        
            vehicleDeath.enabled = false;
            vehicleMove.enabled = false;
            roadComplexity.enabled = false;
            roadGenerator.enabled = false;
            cloudController.enabled = false;
            cameraController.enabled = false;
        }
        
        private void OnDestroy()
        {
            VehicleMainMenu.RestartGenerateVehicleEvent -= vehicleGenerator.Start;
        
            VehicleMainMenu.FindVehicleEvent -= roadGenerator.Start;
            VehicleMainMenu.EnabledEvent -= roadGenerator.Activator;
            VehicleMainMenu.DestroyRoadEvent -= RoadGenerator.DestroyRoad;
        
            VehicleMainMenu.FindVehicleEvent -= roadComplexity.Start;
            VehicleMainMenu.EnabledEvent -= roadComplexity.Activator;
        
            VehicleMainMenu.FindVehicleEvent -= cameraController.Start;
            VehicleMainMenu.EnabledEvent -= cameraController.Activator;
        
            VehicleMainMenu.EnabledEvent -= cloudController.Activator;
        
            VehicleMainMenu.EnabledEvent -= vehicleDeath.Activator;
        
            VehicleMainMenu.FindVehicleEvent -= vehicleMove.Start;
            VehicleMainMenu.EnabledEvent -= vehicleMove.Activator;
        }
    }
}
