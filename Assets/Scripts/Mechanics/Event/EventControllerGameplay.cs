﻿using Gameplay;
using Mechanics.Vehicle;
using UnityEngine;

namespace Mechanics.Event
{
    public class EventControllerGameplay:MonoBehaviour
    {
        public VehicleMoveAndJump vehicleMove;
        public RoadGenerator roadGenerator;
        public RoadComplexity roadComplexity;
        public CameraController cameraController;
        public CloudController cloudController;
        public VehicleDeath vehicleDeath;
        public ScoreAdd scoreAdd;

        public delegate void MethodContainer();

        void Awake()
        {
            VehicleGameplay.EnabledEvent += roadGenerator.ScriptEnabled;
            VehicleGameplay.DisabledEvent += roadGenerator.ScriptDisabled;
            
            VehicleGameplay.EnabledEvent += roadComplexity.ScriptEnabled;
            VehicleGameplay.DisabledEvent += roadComplexity.ScriptDisabled;
            
            VehicleGameplay.EnabledEvent += cameraController.ScriptEnabled;
            VehicleGameplay.DisabledEvent += cameraController.ScriptDisabled;
            
            VehicleGameplay.EnabledEvent += cloudController.ScriptEnabled;
            VehicleGameplay.DisabledEvent += cloudController.ScriptDisabled;
            
            VehicleGameplay.EnabledEvent += vehicleDeath.ScriptEnabled;
            VehicleGameplay.DisabledEvent += vehicleDeath.ScriptDisabled;
            
            VehicleGameplay.EnabledEvent += vehicleMove.ScriptEnabled;
            VehicleGameplay.DisabledEvent += vehicleMove.ScriptDisabled;
            
            VehicleGameplay.EnabledEvent += scoreAdd.ScriptEnabled;
            VehicleGameplay.DisabledEvent += scoreAdd.ScriptDisabled;
        }

        private void OnDestroy()
        {
            VehicleGameplay.EnabledEvent -= roadGenerator.ScriptEnabled;
            VehicleGameplay.DisabledEvent -= roadGenerator.ScriptDisabled;
            
            VehicleGameplay.EnabledEvent -= roadComplexity.ScriptEnabled;
            VehicleGameplay.DisabledEvent -= roadComplexity.ScriptDisabled;
            
            VehicleGameplay.EnabledEvent -= cameraController.ScriptEnabled;
            VehicleGameplay.DisabledEvent -= cameraController.ScriptDisabled;
            
            VehicleGameplay.EnabledEvent -= cloudController.ScriptEnabled;
            VehicleGameplay.DisabledEvent -= cloudController.ScriptDisabled;
            
            VehicleGameplay.EnabledEvent -= vehicleDeath.ScriptEnabled;
            VehicleGameplay.DisabledEvent -= vehicleDeath.ScriptDisabled;
            
            VehicleGameplay.EnabledEvent -= vehicleMove.ScriptEnabled;
            VehicleGameplay.DisabledEvent -= vehicleMove.ScriptDisabled;
            
            VehicleGameplay.EnabledEvent -= scoreAdd.ScriptEnabled;
            VehicleGameplay.DisabledEvent -= scoreAdd.ScriptDisabled;
        }
    }
}
