﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace Core
{
    public abstract class HelpUtils
    {
        public static float Rand(float[] value)=> 
            Random.Range(value[0], value[1]);
        public static int Rand(int[] value)=> 
            Random.Range(value[0], value[1]);
        
        // public static T Rand(List<T> value) where T : class => 
            // Random.Range(value[0], value[value.Count-1]);
            
        public static Sprite RandIndex(List<Sprite> values) => 
            values[Random.Range(0, values.Count)];
        public static GameObject RandIndex(List<GameObject> values) => 
            values[Random.Range(0, values.Count)];

        public static double Pythagoras(Vector2 pos1, Vector2 pos2) =>
            Math.Abs(pos1.magnitude - pos2.magnitude);
        
        public static IEnumerator LoadSceneToString(string sceneToLoad, Animator anim, int joint)
        {
            Time.timeScale = 0;

            anim.SetInteger(joint, 2);
            yield return new WaitForRealSeconds(1.2f);
        
            Time.timeScale = 1;
            SceneManager.LoadScene(sceneToLoad);
        }

        public static float[] ParseColor(Color color) =>
            new[] { color.r, color.g, color.b };

        public static Color ParseColor(float[] color) =>
            new Color(color[0], color[1], color[2]);
        
        
        public static int GetFirstEmpty<T>(T[] array) => Array.IndexOf(array, null);
    }
}