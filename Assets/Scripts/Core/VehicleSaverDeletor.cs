﻿// using System;
// using System.Collections.Generic;
// using UnityEngine;
//
// namespace Core
// {
//     public class VehicleSaverDeletor : MonoBehaviour
//     {
//         public LineRenderer line;
//
//         [HideInInspector] [SerializeField] private List<SerializableVector3> _points;  
//         private float[] _color = new float[3];
//
//         private void Start()
//         {
//             _points = new List<SerializableVector3>();
//
//             for (var i = 0; i < line.positionCount; i++)
//                 _points.Add(line.GetPosition(i));;
//
//             var endColor = line.endColor;
//             _color[0] = endColor.r;
//             _color[1] = endColor.g;
//             _color[2] = endColor.b;
//             
//             // newLine.GetComponent<LineRenderer>().SetPositions(newPos);
//         }
//
//         private void Update()
//         {
//             if (Input.GetKey(KeyCode.S))
//             {
//                 VehicleObj.Current = new VehicleObj(0, "vehicle", _points , _color, 1);
//                 SaveLoadVehicle.SaveVehicle();
//             }
//             if (Input.GetKey(KeyCode.L))
//             {
//                 SaveLoadVehicle.LoadVehicle();
//                 Debug.Log(SaveLoadVehicle.SavedVehicles);
//             }
//         }
//     }
// }