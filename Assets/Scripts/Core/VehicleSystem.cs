﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using Gameplay.Ui;
using UnityEngine;
using UnityEngine.UI;

namespace Core
{
    public class VehiclesSystem
    {
        private static readonly string dataPath = Path.Combine(Application.persistentDataPath, "vehicle.bin");
        private const string STORAGE_KEY = "CurVehicle";

        private readonly VehicleObj[] _savedVehicles;
        private VehicleObj _currentVehicle;

        public VehicleObj Current => _currentVehicle;


        public VehiclesSystem()
        {
            // read exist or create new empty 
            _savedVehicles = File.Exists(dataPath) ? ReadFileData() : new VehicleObj[9];
            string curId = PlayerPrefs.GetString(STORAGE_KEY);
            _currentVehicle = _savedVehicles.FirstOrDefault(a => a.id == curId);
        }

        public void Add(VehicleObj vehicle)
        {
            _currentVehicle = vehicle; // TODO: review this variable
            int firstEmptyIndex = HelpUtils.GetFirstEmpty(_savedVehicles);
            _savedVehicles[firstEmptyIndex] = _currentVehicle;
            UpdateFileData();
            PlayerPrefs.SetString(STORAGE_KEY, _currentVehicle.id);
        }

        public void Destroy(int cellNumber)
        {
            _savedVehicles[cellNumber] = null;
            UpdateFileData();
        }
        
        private VehicleObj[] ReadFileData()
        {
            var bf = new BinaryFormatter();
            using (var file = File.Open(dataPath, FileMode.OpenOrCreate))
                return (VehicleObj[]) bf.Deserialize(file);
        }

        private void UpdateFileData()
        {
            var bf = new BinaryFormatter();
            //Application.persistentDataPath это строка; выведите ее в логах и вы увидите расположение файла сохранений
            using (var file = File.Open(dataPath, FileMode.OpenOrCreate))
                bf.Serialize(file, _savedVehicles);
        }
    }
}