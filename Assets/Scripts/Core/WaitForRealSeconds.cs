﻿using UnityEngine;

namespace Core
{
    public sealed class WaitForRealSeconds : CustomYieldInstruction
    {
        private readonly float _endTime;
 
        public override bool keepWaiting => _endTime > Time.realtimeSinceStartup;

        public WaitForRealSeconds(float seconds) => _endTime = Time.realtimeSinceStartup + seconds;
    }
}