﻿using System;
using UnityEngine;

namespace Core
{
    /// <summary>
    /// Some generic data model, used for List entries
    /// </summary>
    [Serializable]
    public class VehicleObj
    {
        public string id;
        public SerializableVector3[] points;
        public float width;
        public float[] color;

        public byte wheelsCount;

        /// <summary>
        /// Constructor
        /// </summary>
        public VehicleObj(LineRenderer lineVehicle, byte wheelsCount)
        {
            points = new SerializableVector3[lineVehicle.positionCount];
            for (int i = 0; i < lineVehicle.positionCount; i++)
                points[i] = lineVehicle.GetPosition(i);

            id = Guid.NewGuid().ToString();
            this.wheelsCount = wheelsCount;
            color = HelpUtils.ParseColor(lineVehicle.endColor);
            width = lineVehicle.endWidth;
        }
    }
}